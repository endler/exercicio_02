# exercicio_02

Lista de Tarefas.

## Tarefa 01:

- Use Stateful widget e List<String> _tarefas = [‘Dormir’]. Quando o usuário pressionar o botão, o método _tarefas.setState() será chamado, e vai adicionar um novo item na lista. O Flutter irá atualizar a UI.
- Em vez de um texto idêntico, deve receber um texto qualquer em um widget TextField.

## Tarefa 02:

- Construir a lista de tarefas utilizando o ListView.builder(), o qual deverá expor um índice para cada elemento.
- Dado que o elemento tem um índice, implementar alguma forma de excluir uma tarefa da lista. Pode ser feito através de um botão de exclusão ou do gesto de "arrastar para o lado" (gesto comum em aplicações para smartphones).